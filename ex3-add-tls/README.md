# ex3-add-tls

It's good practice to encrypt all point-to-point network communication. We will use [Transport Layer Security (TLS)](https://en.wikipedia.org/wiki/Transport_Layer_Security) to implement our encryption. This is the same technology used by websites to secure their communication with browsers. In this approach, we need a certificate authority that will issue cryptographic certificates to each container on our network. These certificates are first used to establish trust. A container presents its certificate to a container it wants to communicate with. That second container can cryptigraphically verify that the certificate was issued by the trusted certificate authority. Reflexively the second container presents its certificate to the first, and the first can verify that certificate. Once trust is established, each uses a private key and the other's public key (which is part of the certificate) to exchange a session key, which they use for fast encrypted communication.

Although TLS is complicated, most of it can be automated. We need to create a certificate authority (CA) and generate keys and certificates for each container in our system. To do this we'll use [gen-tls](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls). This tool was written for demonstration purposes and should not be used in production (yet). But it will allow us to quickly generate the CA, keys, and certificates that we need.

```bash
cd ex3-add-tls
```

```bash
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server
```

The first part of this command defines the TLS_HOME environment variable that points to a tls subdirectory of the current directory (${PWD}). This variable is temprory, and is only available to the command issued in the same line. The next part of the command calls docker-compose but points it to a specific docker-compose file -- gen-tls.yml -- using the -f option. This file defines a gen-tls service and configures it appropriately. The next part of the command instructs docker compose to run the gen-tls service and remove it (--rm) when it is done. client and server are parameters to the gen-tls service, instructing it to generate certificates and keys for the client and server services.

Running this command we'll see a lot of output. When complete, there will be a tls subdirectory. Explore it, and refer to the [gen-tls documentation](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls).

Now examine docker-compose.yml and client.dockerfile. Notice the extra command parameters relating to tls. These give mongo (for the client) and mongod (for the server) the location of the files they need to communicate using TLS. The `--tls` and `--tlsMode tlsRequired` enable and require tls communication. Also note the `volumes:` entries for the client and server. They mount tls/client and tls/server to /tls in the client and server containers respectively. Notice that the client does not have access to the server's keys, certificates, etc., and vice versa. This is intentional for security puposes. And it also demonstrates that the client and server must somehow exchange information to establish trust and encryption.

Let's run the example.

```bash
docker-compose up --detach
docker-compose logs
```

The output will be similar to that in ex2-add-wait. You might notice a line near the begining similar to the following, which is the configuration of the mongod server and indicates that TLS is being used.

```bash
server_1  | 2020-06-12T18:21:23.092+0000 I  CONTROL  [initandlisten] options: { net: { bindIp: "*", tls: { CAFile: "/tls/ca.pem", certificateKeyFile: "/tls/pem", mode: "requireTLS" } } }
```

But other than than, most of the logs should look the same.

If you want proof that SSL is being used, you can turn off TLS in the client by commenting out the following lines in the client section of the docker-compose.yml file. To comment them out, add the # symbol in front of each line as shown below.

```yml
      # "--tls",
      # "--tlsCAFile", "/tls/ca.pem",
      # "--tlsCertificateKeyFile", "/tls/pem"
```

Save the file and rerun the example.

```bash
docker-compose up --detach
docker-compose logs
```

This will rebuild and restart the client. This time you should see some errors in the log. In particular there should be a line similar to the following.

```bash
server_1  | 2020-06-12T18:27:53.839+0000 I  NETWORK  [conn2] Error receiving request from client: SSLHandshakeFailed: The server is configured to only allow SSL connections. Ending connection from 192.168.112.3:40024 (connection id: 2)
```

This indicates that the server requires TLS (SSL is the old name for TLS), and since the client isn't configured to use TLS, the server rejected the connection.

Let's get things working again. Uncomment the lines you commented in docker-compose.yml. Reload the client and check the logs. Make sure all is working.

Once the system is working again, let's clean up. Besides shutting down and removing the client and server containers (`docker-compse down`), we'll also delete the directory containing the tls files (`rm -r tls`).

```bash
docker-compose down
rm -r tls
```

Uncomment the lines you commented in docker-compose.yml and save the file.

This example is done, so return to the parent directory.

```bash
cd ..
```