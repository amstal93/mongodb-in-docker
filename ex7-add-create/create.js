// Connect to the server. This is similar to account-test-owner.js.
// The difference is that account-test-owner.js runs in the same container
// as the server. This script is evaluated by the client. So we need to tell
// identify the hostname of the MongoDB serve we want to connect to. We have
// given our server the hostname of "server" in our docker-compose.yml.
// We also explicitly specify the port (27017). We don't have to specify the
// port since 27017 is the default port, but we do so to be explicit and to
// demonstrate that it is possible to connect to a MongoDB server that uses
// a different port.
conn = new Mongo("server:27017");

// Use the "test" database.
db = conn.getDB("test");

var password = cat("/secrets/test-owner-password.txt");

// strip leading and trailing whitespace
password = password.replace(/(^\s+|\s+$)/g,'');

db.auth("test-owner", password);

// Here we define a new collection named cats by adding a cat to it.
// MongoDB creates collections when they are first used. We don't have to
// create it first. This is convenient, but error prone. If we misspell a 
// collection (e.g., cots instead of cats), we may get strange behavior
// without an error.
db.cats.insertOne(
  {
    color: "black",
    name: "shadow",
    age: 3
  }
);

// Let's insert many cats at once.
db.cats.insertMany(
  [
    {
      color: "orange",
      name: "sherbert",
      age: 1
    },
    {
      color: "white",
      name: "casper",
      age: 8
    }
  ]
);
